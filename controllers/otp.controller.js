const { getOtpDetails } = require("../middlewares/readDB.middleware");
const { STRINGS, STATUS_CODE_MSG } = require("../utils/statics.utils");
const {
  otpGenerate,
  mobileNoFilter,
  LoggerModule,
} = require('../utils/tools.utils');

/**
 * function to create
 * @param {string} mobileNo
 * @param {string} email
 * @returns {Array} [error,success]
 */

exports.otpCreationController = async ({ mobileNo = null, email = null }) => {
  const otp = otpGenerate();

  const [err, _] = await createOtp({
    field: mobileNo ? mobileNoFilter(mobileNo) : email,
    otp,
  });

  if (err) throw err;

  LoggerModule.info(
    `${STRINGS.OTP_SEND_SUCCESS} for ${mobileNo ?? email} ${otp}`,
  );

  if (mobileNo) await otpSendMessage(otp, mobileNoFilter(mobileNo));

  return `${STRINGS.OTP_SEND_SUCCESS} ${otp}`;
};
/**
 * function to verify otp
 * @param {string} mobileNo
 * @param {string} email
 * @param {string} otp
 * @returns
 */
exports.otpVerificationController = async ({
  mobileNo = null,
  email = null,
  otp,
}) => {
  const [err, succ] = await getOtpDetails({ email, mobileNo, otp });

  if (err)
    throw mergeResponse(STATUS_CODE_MSG.NOT_AUTHORIZED, STRINGS.OTP_FAILED);

  if (!succ) throw STATUS_CODE_MSG.OTP_EXPIRED;

  if (!otpValidate(succ.createdDate)) throw STATUS_CODE_MSG.OTP_EXPIRED;

  LoggerModule.info(`${STRINGS.OTP_VERIFIED_SUCCESS} for ${email ?? mobileNo}`);

  return STRINGS.OTP_VERIFIED_SUCCESS;
};
