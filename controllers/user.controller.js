const {
  responseSuccess,
  LoggerModule,
  responseError,
} = require('../utils/tools.utils');
const {
  otpCreationController,
  otpVerificationController,
} = require('./otp.controller');

exports.userMobileNoGenerate_OTP = async (req, res) => {
  try {
    const {
      query: { mobileNo },
    } = req;

    const succ = await otpCreationController({ mobileNo });

    responseSucess(res, succ);
  } catch (error) {
    LoggerModule.error(`User mobileNo generator Error: ${error?.message || error?.stack || error}`);

    return responseError(res, error);
  }
};

exports.userEmailGenerate_OTP = async (req, res) => {
  try {
    const {
      query: { email },
    } = req;

    const succ = await otpCreationController({ email });

    responseSuccess(res, succ);
  } catch (error) {
    LoggerModule.error(`User email generator Error: ${error?.message || error?.stack || error}`);
   
    return responseError(res, error);
  }
};

exports.userMobileNoVerify_OTP = async (req, res) => {
  try {
    const {
      body: { mobileNo, otp },
    } = req;

    const succ = await otpVerificationController({ mobileNo, otp });

    LoggerModule.info('User MobileNoVerification success');
    return responseSucess(res, succ);
  } catch (error) {
    LoggerModule.error(`User MobileNoVerification Error: ${error?.message || error?.stack || JSON.stringify(error)}`);
    
    return responseError(res, error);
  }
};

exports.userEmailVerify_OTP = async (req, res) => {
  try {
    const {
      body: { email, otp },
    } = req;

    const succ = await otpVerificationController({ email, otp });

    LoggerModule.info('User emailVerification success');
    return responseSucess(res, succ);
  } catch (error) {
    LoggerModule.error(`User emailVerification Error: ${error?.message || error?.stack || error}`);
  
    return responseError(res, error);
  }
};