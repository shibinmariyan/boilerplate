const { verifyUserHelper, createUserHelper, isUser_NotAuthorized, generateUserToken } = require("../helpers/auth.helpers");
const { getUserHelper } = require("../helpers/user.helper");
const { STATUS_CODE_MSG, STRINGS } = require("../utils/statics.utils");
const { mobileNoFilter, encryptPassword, responseSuccess, LoggerModule, responseError, verifyPassword, mergeResponse, loginRE } = require("../utils/tools.utils");

module.exports.userCreationController = async (req, res) => {
    try {

        console.log(req.body)

        const { mobileNo: mobileNumber, email, password } = req.body;
        const mobileNo = mobileNoFilter(mobileNumber)

        const [_, encryptedPwd] = await Promise.all([
          verifyUserHelper(email, mobileNo),
            encryptPassword(password)
        ])
        const userCreation = await createUserHelper({ ...req.body, password: encryptedPwd, mobileNo })

        return responseSuccess(res, userCreation)

    } catch (error) {
        LoggerModule.error(`Error Occured on userCreationController :${error?.message || error?.stack || error}`)
        responseError(res, error)
    }
}
module.exports.userLoginController = async (req, res) => {
    try {
        const { mobileNo: mobileNumber, email, password } = req.body;
        const mobileNo = mobileNoFilter(mobileNumber)
        // pass email or mobileNo as params
        const get_user = await getUserHelper({
            mobileNo,
            email
        });

        const {
            _id: userId,
            name: userName,
            password: userPassword,
            profileImg,
            activity: { WrongAttempts },
        } = get_user;

        const verifyPwd = await verifyPassword(password, userPassword);
        
        if (!verifyPwd) {
            const isNotAuthorized = await isUser_NotAuthorized(get_user);

            if (isNotAuthorized) throw isNotAuthorized;

            throw {
                attempts: +WrongAttempts + 1,
                ...mergeResponse(
                    STATUS_CODE_MSG.INVALID_ENTITY,
                    STRINGS.INVALID_LOGIN
                ),
            }
        }
        //for reseting the wrongAttempts
        isUser_NotAuthorized(get_user, true)

        const { refreshToken, authToken } = generateUserToken({
            userId: userId || "",
            name: userName,
            email,
            mobileNo: mobileNo || "",
        });

        LoggerModule.info(`Account Login success for  userId: ${get_user.userId}`);

        return responseSuccess(res, {
            message: STRINGS.LOGIN_SUCCESS,
            refreshToken,
            authToken,
            user: {
                name: userName,
                email,
                mobileNo: mobileNo || "",
                profileImage: profileImg
            }
        });
    } catch (error) {
        LoggerModule.error(`Error Occured on userLoginController :${error?.message || error?.stack || error}`)
        return loginRE(res, {
            ...error,
            message: error?.message || error?.stack || new Error(error),
            attempts: error?.attempts || 0,
        });
    }
}