const express = require("express");
const {
  QueryValidate_MobileNo,
  QueryValidate_Email,
  BodyValidate_mobileNoOtp,
  BodyValidate_emailOtp,
} = require("../middlewares/requestValidator.middleware");
const {
  userMobileNoGenerate_OTP,
  userEmailGenerate_OTP,
  userMobileNoVerify_OTP,
  userEmailVerify_OTP,
} = require("../controllers/user.controller");
const app = express();

app.get('/mobRqst', QueryValidate_MobileNo, userMobileNoGenerate_OTP);
app.get('/emailRqst', QueryValidate_Email, userEmailGenerate_OTP);
app.post('/mobVerify', BodyValidate_mobileNoOtp, userMobileNoVerify_OTP);
app.post('/emailVerify', BodyValidate_emailOtp, userEmailVerify_OTP);

module.exports = app;
