const express = require("express");
const {
  refresh_verifyToken,
  BodyValidate_createUser,
  BodyValidate_LoginUser,
} = require("../middlewares/requestValidator.middleware");
const { userCreationController, userLoginController } = require("../controllers/auth.controller");

const app = express();

app
  .post('/signup', BodyValidate_createUser, userCreationController)
  .post("/signin", BodyValidate_LoginUser, userLoginController)
// .put("/resetPassword")
// .get("/generateToken", refresh_verifyToken);
module.exports = app;
