const pe = require("parse-error");
const cors = require("cors");
const helmet = require("helmet");
const hpp = require("hpp");
const express = require("express");
const compression = require("compression");
const rateLimit = require("express-rate-limit");
const mongoSanitize = require("express-mongo-sanitize");

const app = express();
const swaggerUi = require("swagger-ui-express");

const {
  LoggerModule,
  responseError,
  isNullOrEmpty,
} = require("./utils/tools.utils");
const { STATUS_CODE_MSG } = require("./utils/statics.utils");
const { CONFIG } = require("./config/config");
// const { swaggerDocs } = require("./swagger/swagger");
const { connectDatabase } = require("./middlewares/database.middleware");

const limit = rateLimit({
  max: 1000, // max requests
  windowMs: 5 * 60 * 1000, // 5 min
  message: "Too many requests", // message to send
});
const port = CONFIG.PORT;

app.use(express.json());
app.use(compression());
app.use(mongoSanitize());
app.use(hpp());
app.use(helmet());
app.set("trust proxy", false);
connectDatabase();

/* handling cors issue */
const whitelist = ['*', '*'];
const corsOptionsDelegate = function (req, callback) {
  let corsOptions;
  const origin = req.header('Origin') || req.header('origin');

  if (whitelist.indexOf(origin) !== -1 || !origin) {
    corsOptions = {
      origin: true,
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    }; // reflect (enable) the requested origin in the CORS response
  } else {
    corsOptions = { origin: false }; // disable CORS for this request
  }

  callback(null, corsOptions); // callback expects two parameters: error and options
};

/* re-routing all the api request to the index route page */
app.use(
  "/apis",
  // cors(corsOptionsDelegate),
  // limit,
  require("./routers/index.router")
);

/* for creating the api documentation */
// app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

/* verifying the url path */

/* to handle the not found apis */
app.use("/", (req, res, next) => {
  // console.log({ req })
  LoggerModule.info(`Invalid request to address : ${req.url} METHOD: ${req.method}`)
  return responseError(res, STATUS_CODE_MSG.API_NOT_FOUND);
});

/* to handle the unhandledRejection from the nodejs */
process.on("unhandledRejection", (error) => {
  LoggerModule.error(pe(error));
  process.exit();
});

process.on("uncaughtException", function (err) {
  // clean up allocated resources
  // log necessary error details to log files
  // exit the process to avoid unknown state
  LoggerModule.error(
    `uncaught Exception: ${err?.message || err?.stack || err}`,
  );
  process.exit();
});

/* listening to the port */
app.listen(port, () => {
  LoggerModule.info(`🚀 project running on port: ${port} 🚀`);
});
