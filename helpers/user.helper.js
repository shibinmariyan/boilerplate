const {
  getUserByEmail,
  getUserBymobileNo,
  getUserById,
} = require("../middlewares/readDB.middleware");
const { STATUS_CODE_MSG, STRINGS, STATUS } = require("../utils/statics.utils");
const { mergeResponse, isNullOrEmptyObj } = require("../utils/tools.utils");

module.exports.getUserHelper = async ({
  mobileNo = null,
  email = null,
  userId = null,
}) => {
  let resolver;

  if (mobileNo) resolver = getUserBymobileNo(mobileNo);
  else if (email) resolver = getUserByEmail(email);
  else if (userId) resolver = getUserById(userId);
  else throw STATUS_CODE_MSG.INTERNAL_ERROR;

  const [err, user] = await resolver;

  if (err) throw err;

  if (isNullOrEmptyObj(user))
    throw mergeResponse(
      [STATUS_CODE_MSG.NO_RECORD_FOUND,
      STRINGS.USER_NOT_EXIST]
    );
  return user;
};
