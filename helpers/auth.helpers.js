const { getUserBymobileNo, getUserByEmail } = require("../middlewares/readDB.middleware");
const { refresh_TokenCreation, auth_TokenCreation } = require("../middlewares/token.middleware");
const { createUser, updateUser_Single } = require("../middlewares/writeToDB.middleware");
const { STATUS_CODE_MSG, STATUS, STRINGS, USERROLE } = require("../utils/statics.utils");
const { isNullOrEmptyObj, LoggerModule, mergeResponse, getObjectId } = require("../utils/tools.utils");

/**
 *
 * @param {string} email
 * @returns {boolean}
 */
const getUser_EmailHelper = async email => {
  const [err, user] = await getUserByEmail(email);

  if (err || isNullOrEmptyObj(user)) return false;

  LoggerModule.warn(`Already email ${email} Exist`);
  return user;
};

/**
 *
 * @param {string} mobileNo
 * @returns {boolean}
 */
const getUser_MobileNoHelper = async mobileNo => {
  const [err, user] = await getUserBymobileNo(mobileNo);

  if (err || isNullOrEmptyObj(user)) return false;

  LoggerModule.warn(`Already mobileNo ${mobileNo} Exist`);
  return user;
};

const verifyUserHelper = async (email, mobileNo) => {
  const [isEmailExist, isMobileExist] = await Promise.all([
    getUser_EmailHelper(email),
    getUser_MobileNoHelper(mobileNo),
  ]);

  if (isEmailExist) throw STATUS_CODE_MSG.EMAIL_EXIST;

  if (isMobileExist) throw STATUS_CODE_MSG.MOB_EXIST;

  return true;
};

const createUserHelper = async ({ mobileNo, name, email, password }) => {

  console.log({ email })

  const [err, succ] = await createUser({
    name,
    password,
    "contactDetails.email": email,
    'contactDetails.mobileNo': mobileNo,
  });
  if (err) {
    LoggerModule.info(`Debugging =>createUserHelper:: ${err}`);
    throw mergeResponse(
      STATUS_CODE_MSG.INTERNAL_ERROR,
      err?.message || err?.stack || err,
    );
  }
  return STATUS_CODE_MSG.CREATED;
};

/**
 * function for adding wrongAttempt Count and to reset
 * @param {object} user
 */
const isUser_NotAuthorized = async (user, reset = false) => {
  const { isLocked, status, WrongAttempts } = user.activity;

  if (isLocked && !reset) throw STATUS_CODE_MSG.ACCOUNT_LOCKED;

  if (status !== STATUS.ACTIVE)
    throw mergeResponse(
      STATUS_CODE_MSG.ACCOUNT_LOCKED,
      STRINGS.ACCOUNT_SUSPEND,
    );

  const [update_err, update_success] = await updateUser_Single(
    {
      userId: getObjectId(user._id),
    },
    {
      $set: {
        updatedDate: new Date(),
        updatedBy: USERROLE.SYSTEM,
        "activity.WrongAttempts": reset ? 0 : WrongAttempts + 1,
        isLocked: !!(WrongAttempts >= 4 && !reset),
      },
    },
  );

  if (update_err) throw update_err;

  return false;
};

const generateUserToken = (user) => {
  const payLoad = {
    userId: user.userId,
    name: user?.name || "",
    email: user?.email || "",
    mobileNo: user?.mobileNo || "",
  };

  const refreshToken = refresh_TokenCreation(payLoad);
  const authToken = auth_TokenCreation(payLoad);

  LoggerModule.info(
    `Token generated for the user: ${user?.firstName || ""} ${user?.lastName || ""
    }`,
  );
  return {
    refreshToken,
    authToken,
  };
};

module.exports = {
  getUser_EmailHelper,
  getUser_MobileNoHelper,
  verifyUserHelper,
  createUserHelper,
  isUser_NotAuthorized,
  generateUserToken
};
