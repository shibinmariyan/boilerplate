// Import required modules and libraries
const bcrypt = require('bcryptjs');
const { to } = require('await-to-js'); // Import async/await helper
const pe = require('parse-error'); // Import parse error handling
const moment = require("moment-timezone"); // Import the moment-timezone library
const mongoose = require('mongoose'); // Import mongoose
const { STRINGS } = require("./statics.utils"); // Import constants
const Logger = require("../config/logger"); // Import the logger module

/**
 * Function to get the local time in a specific timezone
 * @param {string} date - Date to convert to local time
 * @param {string} timeZone - Timezone to convert the date to (default is 'Asia/Calcutta')
 * @returns {string} Local time in the specified timezone
 */
const getLocalTime = (
  date = new Date().toISOString(),
  timeZone = process.env.TIME_ZONE || 'Asia/Calcutta',
) => moment.tz(date, timeZone).utc().format();

/**
 * Function to format time to hours, minutes, and AM/PM
 * @param {string} dat - Date to format
 * @returns {string} Formatted time in 'hh:mm AM/PM' format
 */
const timeFormat = (dat) => {
  const date = getLocalTime(dat);
  let hours = date.getHours();
  let minutes = date.getMinutes();
  const ampm = hours >= 12 ? "PM" : "AM";

  hours %= 12;
  hours = hours || 12;
  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;

  return `${hours}:${minutes} ${ampm}`;
};

/**
 * Function to format a date to 'dd mm yyyy' format
 * @param {string} dat - Date to format
 * @returns {string} Formatted date in 'dd mm yyyy' format
 */
const dateFormat = (dat) => {
  const monthNames = [
    'Jan',
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const date = new Date(dat);
  const dd = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
  const mm = monthNames[date.getMonth()];
  const yy = date.getFullYear();
  return `${dd} ${mm} ${yy}`;
};

/**
 * Function to merge an array of objects into a single object
 * @param {Object[]} objArr - Array of objects to merge
 * @returns {Object} Merged object
 */
const mergeObject = (objArr) => {
  let obj = {};
  objArr.map((x) => {
    obj = Object.assign(obj, x);
  });

  return obj;
};

/**
 * Asynchronously solve a promise and handle errors
 * @param {Promise} promise - Promise to resolve
 * @returns {[Error, any]} Array with an error object and the resolved value
 */
const solvePromise = async (promise) => {
  let err;
  let res;
  [err, res] = await to(promise);

  if (err) return [pe(err)];

  return [null, res];
};

// Export a function to handle an array of promises
const handlePromises = (promises) => {
  // Use Promise.all to concurrently handle multiple promises
  return Promise.all(promises.map(solvePromise)).then((results) => {
    // Find the first error in the results array
    const errorResult = results.find(([err]) => err);

    // If an error is found, throw the first error encountered
    if (errorResult) throw errorResult[0];

    // If no error is found, return the results
    return results;
  });
};

/**
 * Function to handle and send an error response
 * @param {object} res - Response object
 * @param {Error} err - Error object
 * @param {number} attempts - Number of attempts (optional)
 * @returns {object} Response with error information
 */
const responseError = (res, err, attempts) => {
  Logger.error(err);

  return res.status(err?.code || 500).json({
    message: typeof err === "object" ? err.message : err || STRINGS.OOPS,
    ...(attempts ? { attempts } : {}),
  });
};

/**
 * Function to update a process asynchronously
 * @param {Promise} promise - Promise for an update operation
 * @returns {Array} An array with an error message or success message
 */
const updateProcess = async (promise) => {
  const [err, res] = await solvePromise(promise);
  if (err) return [err.message];

  if (res.nModified == 0 || res.nModified != res.n) return [STRINGS.UPDATE_ERROR, null];
  return [null, STRINGS.UPDATE_SUCCESS];
};

/**
 * Function to delete a process asynchronously
 * @param {Promise} promise - Promise for a delete operation
 * @returns {Array} An array with an error message or success message
 */
const deleteProcess = async (promise) => {
  let err;
  let res;
  [err, res] = await solvePromise(promise);

  if (err) return [err.message];

  if (res.deletedCount == 0 || res.deletedCount != res.n) return [STRINGS.DELETE_ERROR, null];
  return [null, STRINGS.DELETED];
};

/**
 * Function to handle login response errors
 * @param {object} res - Response object
 * @param {Error} err - Error object
 */
const loginRE = (res, err) => responseError(res, err, err.attempts || "");

/**
 * Function to send a success response
 * @param {object} res - Response object
 * @param {object|string} data - Data to send as a response
 * @returns {object} Response with success data
 */
const responseSuccess = (res, data) => {
  const send_data = typeof data === "object" ? data : { message: data };
  return res.status(200).json(send_data);
};

/**
 * Function to merge a response object with a message
 * @param {object} obj1 - Original response object
 * @param {string} msg - Message to include in the response
 * @returns {object} Merged response object
 */
const mergeResponse = (obj1, msg) => ({
  code: obj1.code,
  message: msg,
});

/**
 * Function to validate an OTP
 * @param {Date} date - Date to validate
 * @returns {boolean} True if the OTP is valid, false otherwise
 */
const otpValidate = (date) => {
  const differ =
    Math.abs(getLocalTime().getMinutes() - date.getMinutes()) / 1000;
  const min = (differ / 60) % 60;
  if (Math.floor(min) > 8) return false;
  return true;
};

/**
 * Function to format validation error messages
 * @param {Array} errorArray - Array of validation errors
 * @returns {string} Formatted error message
 */
const formatValidationError = (errorArray) => {
  console.log("errorArray", errorArray)
  let errorMessage = "Required ";
  if (errorArray[0].property === "instance")
    errorMessage += errorArray[0].argument;
  else {
    const a = errorArray[0].property.split("instance.")[1];
    errorMessage = `${errorMessage}  ${errorArray[0].argument} #${a}`;
  }
  return errorMessage;
};

/**
 * Function to find the difference between two dates
 * @param {Date} d1 - First date
 * @param {Date} d2 - Second date
 * @returns {string} Difference between the dates in a readable format
 */
const diffDates = (d1, d2) => {
  if (!d2 || Date.parse(d2) == NaN) return "Invalid Date";
  const seconds = Math.floor(getLocalTime(d1) - getLocalTime(d2) / 1000);
  let interval = seconds / 31536000;

  if (interval > 1) return `${Math.floor(interval)} years`;
  interval = seconds / 2592000;

  if (interval > 1) return `${Math.floor(interval)} months`;
  interval = seconds / 86400;

  if (interval > 1) return `${Math.floor(interval)} days`;
  interval = seconds / 3600;

  if (interval > 1) return `${Math.floor(interval)} hours`;
  interval = seconds / 60;

  if (interval > 1) return `${Math.floor(interval)} min`;

  return `${Math.floor(seconds)} sec`;
};

/**
 * Function to check if an object is empty
 * @param {any} obj - Object to check for emptiness
 * @returns {boolean} True if the object is empty, false otherwise
 */
const isNullOrEmptyObj = (obj) => {
  if ([null, undefined].includes(obj) || obj.length == 0) {
    return true;
  }

  if (obj.constructor === Object && Object.keys(obj).length === 0) {
    return true;
  }

  return false;
};

/**
 * Function to check if a string is empty
 * @param {string} str - String to check for emptiness
 * @returns {boolean} True if the string is empty, false otherwise
 */
const isNullOrEmpty = (obj) => {
  if ([null, undefined].includes(obj) || obj.length == 0) return true;

  return false;
};

/**
 * Function to filter a mobile number
 * @param {string} mob - Mobile number to filter
 * @returns {number} Filtered mobile number
 */
const mobileNoFilter = (mob = "") =>
  (`${mob}`.includes("+") ? parseInt(mob.split("+")[1]) : parseInt(mob));

/**
 * Function to check if a string is empty or contains only whitespace
 * @param {string} str - String to check for emptiness
 * @returns {boolean} True if the string is empty or contains only whitespace, false otherwise
 */
const isNullOrEmptyString = (str) => {
  if (
    [null, undefined].includes(str) ||
    str.replace(/^\s+/, "").replace(/\s+$/, "") === ""
  )
    return true;

  return false;
};

/**
 * Function to encrypt a password using bcrypt
 * @param {string} password - Password to encrypt
 * @returns {string} Encrypted password
 */
const encryptPassword = password => bcrypt.hashSync(password, 10);

/**
 * Function to verify a password using bcrypt
 * @param {string} password - Password to verify
 * @param {string} hash - Hashed password to compare
 * @returns {boolean} True if the password is verified, false otherwise
 */
const verifyPassword = (password, hash) => bcrypt.compareSync(password, hash);

/**
 * Function to generate a random OTP (One-Time Password)
 * @returns {number} Randomly generated OTP
 */
const otpGenerate = () => Math.floor(1000 + Math.random() * 9000);

const getObjectId = (objectId) => new mongoose.Types.ObjectId(objectId);

/**
 * Function to format a date with time
 * @param {Date} dat - Date to format
 * @returns {string} Formatted date with time
 */
const dateFormatWithTime = (dat) => `${dateFormat(dat)} ${timeFormat(dat)}`; 

// Export the selected functions and variables
module.exports = {
  getLocalTime,
  timeFormat,
  dateFormat,
  mergeObject,
  solvePromise,
  handlePromises,
  responseError,
  updateProcess,
  deleteProcess,
  loginRE,
  responseSuccess,
  mergeResponse,
  otpValidate,
  formatValidationError,
  diffDates,
  isNullOrEmptyObj,
  isNullOrEmpty,
  mobileNoFilter,
  isNullOrEmptyString,
  encryptPassword,
  verifyPassword,
  otpGenerate,
  getObjectId,
  dateFormatWithTime,
  LoggerModule: Logger,

};
