exports.STATUS = {
  ACTIVE: "ACTIVE",
  NA: "Not Available",
  APPROVED: "APPORVED",
  BLOCKED: "BLOCKED",
  CANCEL: "CANCELLED",
  CLOSE: "CLOSED",
  COMPLETE: "COMPLETED",
  DELIVERED: "DELIVERED",
  EXPIRED: "EXPIRED",
  FAILED: "FAILED",
  INITIATED: "INITIATED",
  INACTIVE: "INACTIVE",
  LOCKED: "LOCKED",
  PENDING: "PENDING",
  REJECTED: "REJECTED",
  SUCESS: "SUCCESS",
  SUSPEND: "SUSPENDED",
};
exports.GENDER = {
  MALE: "MALE",
  FEMALE: "FEMALE",
  OTHER: "OTHER",
};
exports.USERROLE = {
  ADMIN: "admin",
  USER: "user",
  SYSTEM:"system"
};
exports.PATTERNS = {
  ALPHABET: "[a-zA-Z]",
  NUMBER: "[0-9]",
  ALPHANUMERIC: /^[a-zA-Z0-9\s\.,;:'"!?()-]*$/,
  EMAIL: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
  URL: /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/,
};
exports.STATUS_CODE_MSG = {
  SUCESS: {
    code: 200,
    message: "SUCCESS",
  },
  CREATED: {
    code: 200,
    message: "Created Successfully",
  },
  NOTHINGUPDATE: {
    code: 204,
    message: "No Content to Update",
  },
  UPDATE_FAILED: {
    code: 302,
    message: "Update Failed",
  },
  BAD_REQUEST: {
    code: 400,
    message: "Request contains invalid or incomplete data.",
  },
  IMAGE_REQUIRED: {
    code: 400,
    message: "Image Required",
  },
  NOT_AUTHORIZED: {
    code: 401,
    message: "The request for this resource is not authorized.",
  },
  JWT_TOKEN_EXPIRED_AUTH: {
    code: 401,
    message: "TOKEN_EXPIRED",
  },
  INVALID_TOKEN_AUTH: {
    code: 401,
    message: "Invalid_TOKEN",
  },
  JWT_TOKEN_EXPIRED_REFRESH: {
    code: 403,
    message: "TOKEN_EXPIRED",
  },
  AUTH_CREATION_FAILED: {
    code: 403,
    message: "TOKEN creation failed",
  },
  INVALID_TOKEN_REFRESH: {
    code: 403,
    message: "Invalid TOKEN",
  },
  API_NOT_FOUND: {
    code: 404,
    message: "The request entity for this api is not found.",
  },
  NO_RECORD_FOUND: {
    code: 404,
    message: "No Data found.",
  },
  OTP_EXPIRED: {
    code: 410,
    message: "OTP expired",
  },
  EMAIL_EXIST: {
    code: 409,
    message: "Email Already Exists",
  },
  INVALID_ENTITY: {
    code: 422,
    message: "Invalid Entity",
  },
  MOB_EXIST: {
    code: 409,
    message: "MobileNumber Already Exists",
  },
  WRONG_DATA: {
    code: 409,
    message: "Already Exists",
  },
  ACCOUNT_LOCKED: {
    code: 423,
    message: "Sorry your Account is locked, Please reset your password.",
  },
  INTERNAL_ERROR: {
    code: 500,
    message: "Internal error occurred while processing the request",
  },
  CREATION_FAILED: {
    code: 302,
    message: "Creation Failed",
  },
  NOT_COMPLETE: {
    code: 302,
    message: "Not Complete",
  },
};

exports.STRINGS = {
  ACCOUNT_LOCKED: "Sorry your Account is locked, Please reset your password.",
  ACCOUNT_SUSPEND:
    "Sorry your Account is blocked, Please contact the  support team.",
  ALREADY_EXIST: "Already Exist",
  CREATED_SUCCESS: "Created Successfully",
  DELETED: "Resource deleted.",
  DELETE_ERROR: "Error Removing.",
  EMAIL_EXIST: "Email already exist.",
  GENERATED_SUCCESS: "Generated Successfully",
  LOGIN_SUCCESS: "Login Successfully.",
  LOGOUT_SUCCESS: "Logout Successfully.",
  IMAGE_UPLOAD: "Upload an Image",
  INVALID_OTP: "Invalid OTP",
  INVALID_LOGIN: "Invalid Credentials",
  NOT_REGISTER: "User not registerd.",
  NOT_EXIST: "Resource does not exist.",
  NOT_VERIFIED: "Sorry your account is not verified.",
  NUMBER_EXIST: "Mobile Number  already exist.",
  OOPS: "Oops Something went wrong",
  OTP_EXPIRED: "OTP expired",
  OTP_FAILED: "OTP sent failed",
  OTP_SEND_SUCCESS: "OTP sent successfully",
  OTP_VERIFIED_SUCCESS: "OTP verified successfully",
  OTP_VERIFIED_FAIL: "OTP verification failed",
  REGISTER_SUCCESS: "Registration successfully",
  REQUEST_SUCESS: "Request sent successfully",
  USER_NOT_EXIST: "User does not exist.",
  UPDATE_SUCCESS: "Updated successfully.",
  UPDATE_ERROR: "Error updating.",
};
