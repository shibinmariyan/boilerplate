// const { SSMClient, GetParameterCommand } = require("@aws-sdk/client-ssm");
const { SNSClient, PublishCommand } = require("@aws-sdk/client-sns");
// const { SecretsManagerClient, GetSecretValueCommand } = require("@aws-sdk/client-secrets-manager");
const {
  S3Client,
  DeleteObjectCommand,
  GetObjectCommand,
  ListObjectsV2Command,
} = require("@aws-sdk/client-s3");

const multer = require("multer");
const multerS3 = require("multer-s3");
const { getSignedUrl } = require('@aws-sdk/s3-request-presigner');
const { solvePromise, LoggerModule } = require('./tools.utils');

const configure = {
  region: process.env.AWS_REGION,
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_ID,
    secretAccessKey: process.env.AWS_SECRETKEY,
  },
};
const snsClient = new SNSClient({
  ...configure,
  region: process.env.AWS_REGION,
});

const s3client = new S3Client({
  ...configure,
  sslEnabled: false,
  s3ForcePathStyle: true,
  signatureVersion: "v4",
});

const fileSize = {
  image: 1024 * 1024 * 5,
  video: 1024 * 1024 * 500, // 500MB,
  any: 1024 * 1024 * 500,
};

const parameterFetch = (params) => {
  // create a new SSM client
  const client = new SSMClient(configure);

  // create a GetParameter command
  const command = new GetParameterCommand({
    Name: `/Environment/Settings/${params}`,
    // WithDecryption: true // set to false if the parameter value is not encrypted
  });
  return solvePromise(client.send(command));
};

const s3upload = (location="properties", type = "image") => {
  const DBNAME = process.env.S3_BUCKET_NAME;

  const s3 = new S3Client({
    ...configure,
    sslEnabled: false,
    s3ForcePathStyle: true,
    signatureVersion: "v4",
  });

  const allowedFileTypes = {
    image: /\.(jpg|jpeg|png)$/i,
    video: /\.(mp4|avi)$/i,
    // Add more file types as needed
  };

  const getFileTypeErrorMessage = (filetype) => {
    switch (filetype) {
      case "image":
        return "Only image files are allowed!";
      case "video":
        return "Only video files are allowed!";
      default:
        return "Only selected type files are allowed!";
    }
  };

  const fileFilter = (req, file, cb) => {
    const allowedMimeTypes = [
      ...Object.values(allowedFileTypes),
      ...(type === "any" ? ["image/*", ...allowedVideoMimeTypes] : []),
    ];
    if (allowedMimeTypes.some((pattern) => pattern.test(file.originalname))) {
      cb(null, true); // File is allowed
    } else {
      const errorMessage = getFileTypeErrorMessage(type);
      req.fileValidationError = errorMessage;
      cb(new Error(errorMessage), false);
    }
  };

  const generateMetadata = (req, file, cb) => {
    cb(null, {
      fieldName: file.fieldname,
      createdDate: new Date().toString(),
    });
  };

  const generateKey = (req, file, cb) => {
    const re = /(?:\.([^.]+))?$/;
    const ext = re.exec(file.originalname)[1];
    const sanitizedFileName = file.originalname.replace(/\s+/g, "").slice(0, 5);
    const key = `${location}/${Date.now().toString()}_${sanitizedFileName}.${ext}`;
    cb(null, key);
  };
  return multer({
    storage: multerS3({
      s3,
      bucket: DBNAME,
      contentType: multerS3.AUTO_CONTENT_TYPE,
      metadata: generateMetadata,
      key: generateKey,
    }),
    limits: {
      fileSize: fileSize[type],
    },
    fileFilter: fileFilter,
    onError: (err, next) => {
      LoggerModule.error("FILE UPLOAD ERROR", err);
      return err;
    },
  });
};

const deleteFromS3 = (
  params = {
    Key: "",
  },
) => {
  if (!params.Key) return "nothing to delete";

  params.Bucket = process.env.S3_BUCKET_NAME || " ";

  const command = new DeleteObjectCommand(params);
  return s3client.send(command);
};

const getFromS3 = (params) => s3client.send(new GetObjectCommand(params));

const generateS3Url = (bucketName, keyName, region = process.env.AWS_REGION) =>
  `https://${bucketName}.s3.${region}.amazonaws.com/${keyName}`;

const generateSignedURL = async (imageKey) => {
  try {
    const signedUrl = await getSignedUrl(
      s3Client,
      new ListObjectsV2Command({
        Bucket: process.env.S3_BUCKET_NAME,
        Prefix: imageKey,
      }),
      { expiresIn: parseInt(process.env.AWS_SIGNED_URL_EXPIRY, 10) },
    );
    return signedUrl;
  } catch (error) {
    LoggerModule.error("Error generating signed URL:", error);
    throw error;
  }
};

const sendNotification_SMS = async ({ message, number }) => {
  try {
    // Prepare the parameters for the SMS
    const params = {
      Message: message,
      PhoneNumber: `+${number}`,
      // TopicArn: "arn:aws:sns:eu-west-3:253249802692:atease.fifo",
      MessageAttributes: {
        "AWS.SNS.SMS.SMSType": {
          DataType: "String",
          StringValue: "Transactional", // or 'Promotional' for promotional messages
        },
        "AWS.SNS.SMS.SenderID": {
          DataType: "String",
          StringValue: "MK-ATEASE",
        },
      },
    };

    // Send the SMS
    const command = new PublishCommand(params);
    const response = await snsClient.send(command);

    return response.MessageId;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  parameterFetch,
  // secretKeyFetch,
  s3upload,
  deleteFromS3,
  getFromS3,
  generateS3Url,
  snsClient,
  sendNotification_SMS,
  generateSignedURL,
};
