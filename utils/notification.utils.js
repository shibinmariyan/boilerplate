const { PublishCommand } = require("@aws-sdk/client-sns");
const { snsClient, sendNotification_SMS } = require("./aws.utils");

const generateSMS = (
  otp,
) => `${otp} is your one time password to proceed on Atease.
It is valid for 8 minutes. Do not share your OTP with anyone.`;

exports.otpSendMessage = async (otp, number) =>
  sendNotification_SMS({
    message: generateSMS(otp),
    number: number,
  });
