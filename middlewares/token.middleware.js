const jwt = require("jsonwebtoken");
const { CONFIG } = require("../config/config");

module.exports.auth_TokenCreation = (payLoad) =>
  jwt.sign(payLoad, CONFIG.AUTH_PRIVATEKEY, CONFIG.SIGNOPTIONS_AUTH);

module.exports.refresh_TokenCreation = (payLoad) =>
  jwt.sign(payLoad, CONFIG.AUTH_PRIVATEKEY, CONFIG.SIGNOPTIONS_REFRESH);
