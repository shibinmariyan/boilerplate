const { default: mongoose } = require("mongoose");
const { solvePromise } = require("../utils/tools.utils");
const userModels = require("../models/user.models");
const otpModels = require("../models/otp.models");

/**
 * Fetch a single document from a collection based on the provided filter and projector.
 *
 * @param {function} collection - The Mongoose model collection to query.
 * @param {object} filter - The filter criteria to apply to the query.
 * @param {object} projector - The fields to include or exclude in the query result.
 */
const fetch_findOne = (collection, filter, projector = { _id: 0 }) =>
  solvePromise(collection.findOne(filter, projector));

/**
 * Fetch multiple documents from a collection based on the provided filter and projector.
 *
 * @param {function} collection - The Mongoose model collection to query.
 * @param {object} filter - The filter criteria to apply to the query.
 * @param {object} projector - The fields to include or exclude in the query result.
 */
const fetch_findXly = (collection, filter, projector = { _id: 0 }) =>
  solvePromise(collection.find(filter, projector));

/**
 * Fetch documents using aggregation-based pagination from a collection.
 *
 * @param {function} collection - The Mongoose model collection to query.
 * @param {object} filter - The filter criteria to apply to the query.
 * @param {object} option - The pagination options (e.g., limit, page, sortBy).
 */
const fetch_aggregation = (collection, filter, option) =>
  solvePromise(collection.paginate(filter, option));

/**
 * Fetch a user document by email.
 *
 * @param {string} email - The user's email address to find.
 */
module.exports.getUserByEmail = (email) =>
  fetch_findOne(userModels, { "contactDetails.email": email });

/**
 * Fetch a user document by mobile number.
 *
 * @param {string} mobileNo - The user's mobile number to find.
 */
module.exports.getUserBymobileNo = (mobileNo) =>
  fetch_findOne(userModels, { "contactDetails.mobileNo": +mobileNo });

/**
 * Fetch a user document by user ID.
 *
 * @param {string} userId - The user's unique ID to find.
 */
module.exports.getUserById = (userId) =>
  fetch_findOne(userModels, { _id: mongoose.Schema.ObjectId(userId) });

/**
 * Fetch multiple user documents based on the provided filter criteria.
 *
 * @param {object} filter - The filter criteria to apply to the query.
 */
module.exports.getUsersAll = (filter = {}) => fetch_findXly(userModels, filter);

/**
 * Fetch OTP details based on email, mobile number, and OTP value.
 *
 * @param {object} params - The object containing email, mobileNo, and OTP.
 */
module.exports.getOtpDetails = ({ email = null, mobileNo = null, otp }) =>
  fetch_findOne(otpModels, { field: mobileNo ?? email, otp: +otp });
