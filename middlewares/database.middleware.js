// Import the 'mongoose' library
const { default: mongoose } = require("mongoose");

// Import the 'getLocalTime' and 'LoggerModule' functions from the 'tools.utils' module
const { getLocalTime, LoggerModule } = require("../utils/tools.utils");

// Function to connect to the MongoDB database
exports.connectDatabase = () =>
  new Promise((resolve, reject) => {
    // Construct the MongoDB connection URI based on environment variables
    const url = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_URL}/${process.env.DB_NAME}-${process.env.MODE}?retryWrites=true&w=majority`
    console.log(url)
    // Set Mongoose's promise library to the global promise library
    mongoose.Promise = global.Promise;

    // Configure MongoDB connection options and establish a connection
    mongoose
      .connect(`${url}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        maxPoolSize: 10,
        connectTimeoutMS: 10000,
        wtimeoutMS: 2500,
        bufferCommands: false, // Disable mongoose buffering
      })
      .then(() => {
        LoggerModule.info("Connected to MongoDB  🏹 "); // Log a successful connection message
      })
      .catch((err) => {
        LoggerModule.error("Error connecting to MongoDB:", err); // Log an error if the connection fails
      });

    // Handle MongoDB connection events
    mongoose.connection
      // Reject if an error occurred when trying to connect to MongoDB
      .on("error", (error) => {
        LoggerModule.error("Error: connection to DB failed");
        reject(error);
      })
      // Exit the process if there is no longer a Database Connection
      .on("close", () => {
        LoggerModule.error("Error: Connection to DB lost");
        process.exit(1);
      })
      // Connected to DB
      .once("open", () => {
        // Resolve the promise with a success message and the current time
        resolve(`DB Connected on ${getLocalTime()}`);
      });
  });
