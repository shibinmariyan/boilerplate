const { Validator } = require('jsonschema');
const {
  userCreateSchema,
  userLoginSchema,
  mobileNoOtpSchema,
  emailOtpSchema,
} = require('./jsonSchema.middleware');

const valid = new Validator();
const jwt = require('jsonwebtoken');
const { CONFIG } = require('../config/config');
const { STATUS_CODE_MSG } = require('../utils/statics.utils');
const { responseError, mergeResponse, LoggerModule, formatValidationError, isNullOrEmptyObj } = require('../utils/tools.utils');

exports.BodyValidate_createUser = (req, res, next) =>
  validation_Body(req, res, next, userCreateSchema);

exports.BodyValidate_LoginUser = (req, res, next) =>
  validation_Body(req, res, next, userLoginSchema);

exports.BodyValidate_mobileNoOtp = (req, res, next) =>
  validation_Body(req, res, next, mobileNoOtpSchema);

exports.BodyValidate_emailOtp = (req, res, next) =>
  validation_Body(req, res, next, emailOtpSchema);

exports.QueryValidate_MobileNo = (req, res, next) =>
  validation_Query(req, res, next, mobileNoSchema);

exports.QueryValidate_Email = (req, res, next) =>
  validation_Query(req, res, next, emailSchema);



exports.validationImage = (req, res, next) => {
  const { files } = req;
  try {
    if (!isNullOrEmptyObj(files)) {
      //eliminating array for easy accessing only works with single file upload
      let keys = Object.keys(files);

      if (keys.length < 1)
        throw STATUS_CODE_MSG.IMAGE_REQUIRED


      const id = keys[0]
      files[id] = files[id][0];

      return next();
    } throw STATUS_CODE_MSG.IMAGE_REQUIRED;
  } catch (error) {
    LoggerModule.error(`validationImage Error: ${error?.message || error?.stack || error}`);

    return responseError(res, error);
  }
}
exports.validationImageOptional = (req, res, next) => {
  const { files } = req;
  try {
    if (files && !isNullOrEmptyObj(files) && Object.keys(files).length > 0) {
      let id = Object.keys(files)[0];
      let data = files[id][0];
      files[id] = {
        ...data,
      };
    }
    else req.files = { image: { path: null } };

    return next();

  } catch (error) {
    LoggerModule.error(`validationImageOptional Error: ${error?.message || error?.stack || error}`);

    return responseError(res, error);
  }
};
exports.auth_verifyToken = (req, res, next) => {
  let token = req.headers.authorization;

  if (!token) {
    LoggerModule.error('Auth Verification Error : \'No Token found in the header')
    return responseError(res, STATUS_CODE_MSG.INVALID_TOKEN_AUTH)
  }
  token = token.split(' ')[1];

  jwt.verify(token, CONFIG.AUTH_PUBLICKEY, CONFIG.SIGNOPTIONS_AUTH, (err, user) => {

    if (err || !user) {
      LoggerModule.error(`Auth Verification Error : ${err?.message || err?.stack || err} with token: ${token}`)
      return responseError(res, STATUS_CODE_MSG.JWT_TOKEN_EXPIRED_AUTH)
    }
    req.user = user;
    LoggerModule.info(`token verified for the user :${user.userId} via webapp`);
    next();
  },
  );
};
exports.refresh_verifyToken = (req, res, next) => {
  const token = req.headers.refreshtoken

  if (!token) return responseError(res, STATUS_CODE_MSG.INVALID_TOKEN_REFRESH);
  jwt.verify(token, CONFIG.AUTH_PUBLICKEY, CONFIG.SIGNOPTIONS_REFRESH, (err, user) => {
    if (err) {
      Logger.error(err)
      return responseError(res, STATUS_CODE_MSG.JWT_TOKEN_EXPIRED_REFRESH)
    }
    req.user = user;
    LoggerModule.info('Verified token', user.userId);
    next();
  },
  );
};

/**
 *
 * @param {object} req -request from the client.
 * @param {object} res - response of the client
 * @param {function} next - next as callback
 * @param {object} model - jsonschema model for validating the params
 */
const validation_Body = (req, res, next, model) =>
  validate(valid.validate(req.body, model).errors, res, next);

/**
 *
 * @param {object} req -request from the client.
 * @param {object} res - response of the client
 * @param {function} next - next as callback
 * @param {object} model - jsonschema model for validating the params
 */
const validation_Query = (req, res, next, model) =>
  validate(valid.validate(req.query, model).errors, res, next);

/**
 * validate will check whether the validRes is empty array or not, if not then it
 * will through an error with the data of validRes else move to the next()
 * @param {object} validRes - will contains all the invalid params
 * @param {object} res  - response of the request
 * @param {function} next  - next as callback

 */
const validate = (validRes, res, next) => {
  if (!(validRes.length > 0)) return next();

  LoggerModule.error(`Parameter Validate Error: ${validRes}`);
  return responseError(res,
    mergeResponse(STATUS_CODE_MSG.BAD_REQUEST, `${formatValidationError(validRes)}`)
  );
};
