const { STATUS_CODE_MSG } = require("../utils/statics.utils");
const { responseError } = require("../utils/tools.utils");

exports.isAuthorizedAdmin = (req, res, next) => {
  // needs to modify this code based on the project architecture
  const {
    user: { userId = null },
  } = req;

  const isAdmin = !!userId.includes('user');
  if (isAdmin) {
    next();
  } else {
    return responseError(res, STATUS_CODE_MSG.NOT_AUTHORIZED);
  }
};
