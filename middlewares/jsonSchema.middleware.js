const {
  nameModel,
  passwordModel,
  emailModel,
  mobileNoModel,
  otpModel,
} = require("../models/jsonschema.models");

exports.userCreateSchema = {
  properties: {
    name: nameModel,
    password: passwordModel,
    email: emailModel,
    mobileNo: mobileNoModel,
  },
  required: ["name", "mobileNo", "email", "password"],
};
exports.userLoginSchema = {
  properties: {
    password: passwordModel,
    // email: emailModel,
    mobileNo: mobileNoModel,
  },
  required: ["mobileNo", "password"],
};

exports.mobileNoSchema = {
  properties: {
    mobileNo: mobileNoModel,
  },
  required: ["mobileNo"],
};

exports.emailSchema = {
  properties: {
    email: emailModel,
  },
  required: ["email"],
};

exports.mobileNoOtpSchema = {
  properties: {
    mobileNo: mobileNoModel,
    otp: otpModel,
  },
  required: ["mobileNo", "otp"],
};

exports.emailOtpSchema = {
  properties: {
    email: emailModel,
    otp: otpModel,
  },
  required: ["email", "otp"],
};
