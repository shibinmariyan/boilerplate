/**
 * creation
 * @param {object} data - will contain all the data to be saved.
 */

const otpModels = require("../models/otp.models");
const userModels = require("../models/user.models");
const { solvePromise, updateProcess } = require("../utils/tools.utils");

module.exports.createUser = data => solvePromise(new userModels(data).save());

module.exports.createOtp = data => solvePromise(new otpModels(data).save());

/**
 * updation
 * @param {object} cred - will contain all the find condition data
 * @param {object} setData - will contain all the data to update will type eg: {$set:{},$push:{}}
 */

module.exports.updateUser_Single = (cred, setData) =>
  updateProcess(userModels.updateOne(cred, setData));
