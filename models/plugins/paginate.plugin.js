module.exports = function paginationPlugin(schema) {
  // Add a static method 'paginate' to the schema
  schema.statics.paginate = async function (filter, options) {
    // Extract limit and page options or use defaults
    const limit = options.limit || 10;
    const page = options.page || 1;

    // Calculate the number of documents to skip based on the page and limit
    const skip = (page - 1) * limit;

    // Count the total number of documents matching the filter
    const countPromise = this.countDocuments(filter).exec();

    // Initialize the aggregation pipeline
    const pipeline = [];

    // Add a 'match' stage to the pipeline based on the filter
    if (filter) {
      pipeline.push({ $match: filter });
    }

    // Add a 'sort' stage to the pipeline based on 'sortBy' option
    if (options.sortBy) {
      const sortStage = {};
      options.sortBy.split(",").forEach((sortOption) => {
        const [key, order] = sortOption.split(":");
        sortStage[key] = order === "desc" ? -1 : 1;
      });
      pipeline.push({ $sort: sortStage });
    }

    // Add 'skip' and 'limit' stages to implement pagination
    pipeline.push({ $skip: skip });
    pipeline.push({ $limit: limit });

    // Execute the aggregation with the pipeline
    const docsPromise = this.aggregate(pipeline).exec();

    // Combine the promises to obtain the total results and documents
    return Promise.all([countPromise, docsPromise]).then((values) => {
      const [totalResults, results] = values;
      // Calculate the total number of pages
      const totalPages = Math.ceil(totalResults / limit);
      // Create the result object
      const result = {
        results,
        page,
        limit,
        totalPages,
        totalResults,
      };
      return Promise.resolve(result);
    });
  };
};
