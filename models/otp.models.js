// Import the 'mongoose' library
const { default: mongoose } = require("mongoose");

// Import the 'getLocalTime' function from the 'tools.utils' module
const { getLocalTime } = require("../utils/tools.utils");

// Define the 'otpSchema' schema using Mongoose
const otpSchema = new mongoose.Schema(
  {
    otp: {
      type: Number,
      required: true,
    },
    field: {
      type: String,
      required: true,
    },
    createdDate: {
      type: Date,
      default: getLocalTime(), // Set the default value for the 'createdDate' field using the 'getLocalTime' function
      expires: '60', // Automatically expire documents after 60 seconds
    },
  },
  {
    timestamps: false, // Disable Mongoose timestamps (createdAt and updatedAt fields)
  },
);

// Create and export a Mongoose model named 'otp' with the 'otpSchema'
module.exports = mongoose.model('otp', otpSchema);
