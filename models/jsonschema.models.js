const { PATTERNS, GENDER, OFFENSE, STATUS } = require("../utils/statics.utils");

exports.dobModel = {
  type: "string",
  format: "dd mmm yyyy",
};

exports.emailModel = {
  type: "string",
  format: "email",
  pattern: PATTERNS.EMAIL,
};

exports.nameModel = {
  type: "string",
  maxLength: 50,
  minLength: 3,
  pattern: PATTERNS.ALPHABET,
};

exports.genderModel = {
  type: "string",
  pattern: PATTERNS.ALPHABET,
  enum: GENDER,
};

exports.mobileNoModel = {
  type: "string",
  pattern: PATTERNS.NUMBER,
  maxLength: 15,
  minLength: 5,
};

exports.otpModel = {
  type: "string",
  pattern: PATTERNS.NUMBER,
  maxLength: 6,
  minLength: 6,
};

exports.passwordModel = {
  type: "password",
};

exports.dateModel = {
  type: "string",
  format: "dd mmm yyyy",
};

exports.timeModel = {
  type: "string",
  format: "HH mm ss",
};

exports.stringModel = {
  type: "string",
};
exports.numberModel = {
  type: "string",
  pattern: PATTERNS.NUMBER,
};

exports.addressModel = {
  type: "string",
  pattern: PATTERNS.ALPHANUMERIC,
  minLength: 10,
  maxLength: 150,
};

exports.descriptionModel = {
  type: "string",
  pattern: PATTERNS.ALPHANUMERIC,
  minLength: 10,
  maxLength: 2000,
};
exports.cityModel = {
  type: "string",
  pattern: PATTERNS.ALPHABET,
};

exports.postalModel = {
  type: "string",
  pattern: PATTERNS.NUMBER,
};
exports.statusModel = {
  type: "string",
  enum: [STATUS.PENDING, STATUS.APPROVED, STATUS.REJECTED],
};
exports.objectModel = {
  type: "string",
  minLength: 24,
  maxLength: 24,
  // pattern: PATTERNS.OBJECTID  need to add objectId
};
