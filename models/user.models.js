const { default: mongoose } = require("mongoose");
const { STATUS, STRINGS, GENDER, USERROLE } = require("../utils/statics.utils");
const { getLocalTime } = require("../utils/tools.utils");
const paginationPlugin = require('./plugins/paginate.plugin')
const userSchema = new mongoose.Schema(
  {
    //enable only if custom Id is required
    // userId: {   
    //   // unique userId will be stored
    //   type: String,
    //   unique: true,
    //   required: true,
    // },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    dob: {
      default: new Date(),
      type: String,
    },
    password: {
      // encrypted password will be stored
      type: String,
      trim: true,
      required: true,
    },
    profileImg: {
      // profile Picture Path will be stored
      type: String,
      default: null,
      trim: true,
    },
    gender: {
      // gender will be stored
      type: String,
      default: GENDER.OTHER,
      enum: Object.values(GENDER),
    },
    // for monitoring purpose starts
    createdBy: {
      // to iden tify the people created and generated
      type: String,
      default: USERROLE.USER,
      enum: Object.values(USERROLE),
    },
    updatedBy: {
      // to identify the  updated person stores the userId
      type: String,
      trim: true,
    },
    // for monitoring purpose ends
    contactDetails: {
      email: {
        // email of the user it will be unique
        type: String,
        lowercase: true,
        trim: true,
        required: true,
        unique: `${STRINGS.EMAIL_EXIST}`,
      },
      mobileNo: {
        // mobileNo of the user it will be unique
        type: Number,
        unique: `#${STRINGS.NUMBER_EXIST}`,
        required: true,
      },
      location: {
        // all data will be given from the frontend with help of google map api
        longitude: {
          type: String,
          trim: true,
        },
        latitude: {
          type: String,
          trim: true,
        },
        subLocality: {
          // main one === city
          type: String,
          trim: true,
        },
        locality: {
          type: String,
          trim: true,
        },
        subArea: {
          type: String,
          trim: true,
        },
        state: {
          type: String,
          trim: true,
        },
        country: {
          type: String,
          trim: true,
        },
      },
    },

    activity: {
      isLocked: {
        // to block the user from actvities if find any fraud activity
        type: Boolean,
        default: false,
        enum: [true, false],
      },
      WrongAttempts: {
        // for storing the incorrect password
        type: Number,
        default: 0,
      },
      status: {
        /// / status of the user
        type: String,
        default: STATUS.ACTIVE,
        enum: [STATUS.ACTIVE, STATUS.LOCKED, STATUS.SUSPEND],
      },
      lastLogin: {
        // to update the last login time by the user
        type: Date,
        default: getLocalTime(),
      },
    },
  },
  {
    timestamps: true,
  },
);
userSchema.plugin(paginationPlugin);

module.exports = mongoose.model("user", userSchema);
