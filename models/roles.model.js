const { default: mongoose } = require("mongoose");


const roleSchema = mongoose.Schema(
    {
      roleName: {
        type: String,
        required: true,
        trim: true,
        unique: true,
      },
      description: {
        type: String,
        max:100,
        min:3
      },
    },
    {
      timestamps: true,
    }
  );