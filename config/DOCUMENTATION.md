
## Overview

This script generates a public and private RSA key pair and stores them in separate files. It is intended to be used for cryptographic purposes, and it uses the Node.js `crypto` module.

## Usage

To run the script, follow these steps:

1. Create a JavaScript file (e.g., `key_generator.js`) in your project directory.

2. Install the required Node.js modules (if not already installed) by running the following command:

   ```shell
   npm install crypto || yarn add -D crypto
   ```
3. Run the script using Node.js:

   ```shell
   node key_generator.js
   ```

## Key Generation

The script generates an RSA key pair with the following settings:

- Modulus Length: 2048 bits (adjustable)
- Public Key Format: PEM
- Private Key Format: PEM

## Generated Files

The script saves the generated keys in two separate files:

- **Public Key**: `public_key.key`
- **Private Key**: `private_key.key`

Make sure to keep the private key (`private_key.key`) secure and do not share it with others, as it is used for encryption and decryption of sensitive data.

## Customization

You can customize the key length and file paths by modifying the script code to meet your specific requirements.

## Note

It's important to handle the keys securely, especially the private key. If you lose the private key, you may lose the ability to decrypt data encrypted with the public key.

For further documentation on the Node.js `crypto` module, you can refer to the official Node.js documentation:

- [Node.js `crypto` Module Documentation](https://nodejs.org/api/crypto.html)
```
