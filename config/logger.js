// Import required modules
const winston = require("winston");
const path = require("path");
const { SPLAT } = require("triple-beam");
const appRoot = require("app-root-path");

const { combine, timestamp, colorize, printf } = winston.format;
// Define the project root directory
const PROJECT_ROOT = path.join(__dirname, "..");

// Define log options for file and console transports
const options = {
  file: {
    level: "info",
    filename: `${appRoot}/logs/${new Date().toDateString()}_app.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 15,
    colorize: true,
    timestamp: true,
  },
  console: {
    level: "debug",
    handleExceptions: true,
    json: true,
    colorize: true,
    timestamp: true,
  },
};

// Define a custom log format
const customFormat = printf((info) => {
  const { level, message, timestamp: infoTimeStamp } = info;
  const rest = info[SPLAT] || [];
  const msg = info.stack ? formatObject(info.stack) : formatObject(message);
  let result = `${infoTimeStamp} - ${level}: ${msg}`;

  if (rest.length) {
    result += `\n${rest.map(formatObject).join("\n")}`;
  }

  return result;
});

// Function to format an object as a string
function formatObject(param) {
  if (typeof param === "string") {
    return param;
  }

  if (param instanceof Error) {
    return param.stack ? param.stack : JSON.stringify(param, null, 2);
  }

  return JSON.stringify(param, null, 2);
}

// Create a Winston logger with specified options and transports
const logger = winston.createLogger({
  format: combine(
    timestamp(),
    colorize({
      colors: {
        audit: "magenta",
        trace: "white",
        perf: "green",
      },
    }),
    customFormat,
  ),
  transports: [
    new winston.transports.File(options.file), // File transport
    new winston.transports.Console(options.console), // Console transport
  ],
  exitOnError: false, // Do not exit on handled exceptions
});

// Define a custom 'stream' to handle log entries
logger.stream = {
  write: function (message) {
    // Use the 'info' log level so the output is picked up by both transports (file and console)
    logger.info(message);
  },
};

// Export logging functions and the logger 'stream'
module.exports.debug = module.exports.log = function () {
  logger.debug.apply(logger, formatLogArguments(arguments));
};
module.exports.info = function () {
  logger.info.apply(logger, formatLogArguments(arguments));
};
module.exports.warn = function () {
  logger.warn.apply(logger, formatLogArguments(arguments));
};
module.exports.error = function () {
  logger.error.apply(logger, formatLogArguments(arguments));
};
module.exports.stream = logger.stream;

// Function to format log arguments
function formatLogArguments(args) {
  args = Array.prototype.slice.call(args);

  const stackInfo = getStackInfo(1);

  if (stackInfo) {
    // Get file path relative to the project root
    const calleeStr = '(' + stackInfo.relativePath + ':' + stackInfo.line + ')';

    if (typeof args[0] === "string") {
      args[0] = `${calleeStr} ${args[0]}`;
    } else {
      args.unshift(calleeStr);
    }
  }

  return args;
}

// Function to extract stack information
function getStackInfo(stackIndex) {
  // Get call stack and analyze it
  // Get all file, method, and line numbers
  const stacklist = new Error().stack.split('\n').slice(3);

  // Stack trace format:
  // http://code.google.com/p/v8/wiki/JavaScriptStackTraceApi
  // Do not remove the regex expressions from outside of this method (due to a BUG in node.js)
  const stackReg = /at\s+(.*)\s+\((.*):(\d*):(\d*)\)/gi;
  const stackReg2 = /at\s+()(.*):(\d*):(\d*)/gi;

  const s = stacklist[stackIndex] || stacklist[0];
  const sp = stackReg.exec(s) || stackReg2.exec(s);

  if (sp && sp.length === 5) {
    return {
      method: sp[1],
      relativePath: path.relative(PROJECT_ROOT, sp[2]),
      line: sp[3],
      pos: sp[4],
      file: path.basename(sp[2]),
      stack: stacklist.join("\n"),
    };
  }
}
