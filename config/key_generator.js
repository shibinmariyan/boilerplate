const fs = require("fs");
const crypto = require("crypto");

// Generate a new RSA key pair
const { publicKey, privateKey } = crypto.generateKeyPairSync("rsa", {
  modulusLength: 2048, // Adjust the key length as needed
  publicKeyEncoding: {
    type: "spki",
    format: "pem",
  },
  privateKeyEncoding: {
    type: "pkcs8",
    format: "pem",
  },
});

const { MODE } = process.env;
// Paths for the public and private key files
const publicKeyPath = `./config/${MODE}.public_key.key`;
const privateKeyPath = `./config/${MODE}.private_key.key`;

// Save the public key to a file
fs.writeFileSync(publicKeyPath, publicKey);
console.log("Public key saved to", publicKeyPath);

// Save the private key to a file
fs.writeFileSync(privateKeyPath, privateKey);
console.log("Private key saved to", privateKeyPath);
