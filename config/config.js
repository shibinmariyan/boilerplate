// Import necessary modules
const fs = require("fs");
const { mergeObject } = require("../utils/tools.utils");

// Define a set of constant options
const OPTIONS = {
  SIGNOPTIONS: {
    issuer: "mih",
    subject: "info@mih.com",
    audience: "xmedApp",
  },
  algorithms: {
    algorithm: "RS256",
  },
  verifyAlgorithm: {
    algorithm: ["RS256"],
  },
  refreshExpires: {
    expiresIn: process.env.MODE === "development" ? "10d" : "7d",
  },
  authExpires: {
    expiresIn: process.env.MODE === "development" ? "24hr" : "4hr",
  },
};

// Export a configuration object
exports.CONFIG = {
  PORT: process.env.PORT || 3080, // Set the server port based on the environment variable or use a default value
  AUTH_PRIVATEKEY: fs.readFileSync(
    `${__dirname}/${process.env.MODE}.private_key.key`,
    "utf-8",
  ), // Read the private key from a file based on the environment mode
  AUTH_PUBLICKEY: fs.readFileSync(
    `${__dirname}/${process.env.MODE}.public_key.key`,
    "utf-8",
  ), // Read the public key from a file based on the environment mode
  SIGNOPTIONS_REFRESH: mergeObject([
    // Merge the signing options for refresh tokens
    OPTIONS.SIGNOPTIONS,
    OPTIONS.algorithms,
  ]),
  SIGNOPTIONS_AUTH: mergeObject([
    // Merge the signing options for authentication tokens
    OPTIONS.SIGNOPTIONS,
    OPTIONS.algorithms,
    OPTIONS.authExpires,
  ]),
  PAYLOAD_ENCRYPT_OPTIONS: mergeObject([
    // Merge the payload encryption options
    OPTIONS.SIGNOPTIONS,
    OPTIONS.algorithms,
  ]),
};

// Export an empty array for storing refresh tokens
exports.refreshToken_Stack = [];
